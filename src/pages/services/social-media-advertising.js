import React, { useState, useRef, useEffect } from "react";
import Link from "next/link";
import Image from "components/Image";
import Layout from "components/Layout";
import styles from "../../styles/pages/Social-media-management.module.scss";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from "react-accessible-accordion";
import "react-accessible-accordion/dist/fancy-example.css";
import { AiOutlineRight } from "react-icons/ai";

var $ = require("jquery");
if (typeof window !== "undefined") {
  window.$ = window.jQuery = require("jquery");
}

import dynamic from "next/dynamic";
const OwlCarousel = dynamic(() => import("react-owl-carousel"), {
  ssr: false,
});

import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

const Socialmediaadvertising = () => {
  const [shown, setIsShown] = useState(null);
  const [questions, setIsQuestions] = useState(false);

  const handleIsActive = (value) => {
    setIsShown(value);
  };
  const activeQuestions = (value) => {
    setIsQuestions(value);
  };
  const faqs = [
    {
      title: " How long does it take to see results",
      description:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      title: "Do I have to be local to work with you?",
      description:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      title: "Are there any hidden agency fees?",
      description:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      title: "How profitable are Facebook ads?",
      description:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      title: "How much do I need to spend on Facebook?",
      description:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      title: "06. Will I be locked in a contract?",
      description:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      title: "07. Does AM Group create my content?",
      description:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
  ];
  const packeges = [
    {
      title: "Understand Your Business",
      content:
        "You will receive a Social Media Manager, monthly content strategy, x6 weekly posts, content calendar, community management, monthly call, performance reports and 24/7 acces to a live analytics dashboard.",
    },
    {
      title: "Define Target Market",
      content:
        "You will receive a Social Media Manager, monthly content strategy, x6 weekly posts, content calendar, community management, monthly call, performance reports and 24/7 acces to a live analytics dashboard.",
    },
    {
      title: "Competitive Analysis",
      content:
        "You will receive a Social Media Manager, monthly content strategy, x6 weekly posts, content calendar, community management, monthly call, performance reports and 24/7 acces to a live analytics dashboard.",
    },
    {
      title: "Set Objectives",
      content:
        "You will receive a Social Media Manager, monthly content strategy, x6 weekly posts, content calendar, community management, monthly call, performance reports and 24/7 acces to a live analytics dashboard.",
    },
    {
      title: "Build Ads Strategy ",
      content:
        "You will receive a Social Media Manager, monthly content strategy, x6 weekly posts, content calendar, community management, monthly call, performance reports and 24/7 acces to a live analytics dashboard.",
    },
    {
      title: "Reporting & Optimisation",
      content:
        "You will receive a Social Media Manager, monthly content strategy, x6 weekly posts, content calendar, community management, monthly call, performance reports and 24/7 acces to a live analytics dashboard.",
    },
  ];

  const logoSlider = {
    loop: false,
    margin: 20,
    // items: 4,
    nav: false,
    dots: false,
    responsiveClass: true,
    responsive: {
      0: {
        items: 4,
        dots: false,
        loop: false,
      },
      600: {
        items: 4,
        dots: false,
        loop: false,
      },
      1000: {
        items: 4,
        dots: false,
        loop: false,
      },
    },
  };

  const WebsitesLogo = [
    {
      logoImage: "/icons/adveretising/messanger.svg",
    },
    {
      logoImage: "/icons/adveretising/insta.svg",
    },
    {
      logoImage: "/icons/adveretising/fb-market.svg",
    },
    {
      logoImage: "/icons/adveretising/tiktok.svg",
    },
  ];

  return (
    <>
      <Layout>
        <section className="banner services_banner common_padding">
          <div className="container">
            <div className="banner_content">
              <div className="row align-items-center">
                <div className="col-lg-8 col-md-8 col-md-12 common_title">
                  <p>
                    Paid <span className="fw-bold"> Marketing</span>
                  </p>
                  <h1>
                    Build Ads With
                    <br /> <span className="orange_color fw-bold">Explosive Results</span>
                  </h1>
                </div>
                <div className="col-lg-4 col-md-4 col-md-12 services_content_right">
                  <p>
                    Revolutionise the way you grow your business. We aren't just talking about that 'Boost Post' button
                    either. We are talking about a strategic content-responsive, data-driven approach.
                  </p>
                </div>
              </div>
            </div>
            <div className="banner_image_column">
              <img src="/images/advertising-img.jpg"></img>
            </div>
          </div>
        </section>

        <div className="common_padding text-center pt-0">
          <div className="container">
            <OwlCarousel responsiveClass="true" {...logoSlider}>
              {WebsitesLogo.map((logo, index) => (
                <div className="logo_slider" key={index}>
                  <Image src={logo.logoImage} width={90} height={65} alt="icon"></Image>
                </div>
              ))}
            </OwlCarousel>
          </div>
        </div>

        <section className="packeges_sec common_padding pb-0" id="marketing_packages">
          <div className="container">
            <div className="packeges_top common_title">
              <p>
                Our<span className="fw-bold"> Approach</span>
              </p>
              <h2>
                We Build Ads With
                <br />
                <span className="fw-bold">Explosive Results.</span>
              </h2>
            </div>
            <div className="row common_padding">
              {packeges &&
                packeges.map((item, index) => (
                  <div className=" col-md-4 mb-2" key={index}>
                    <div className="advertising_results ">
                      <div className="d-flex justify-content-between pb-2">
                        <div className="flex_center common_title">
                          <h3>
                            <span className="orange_color">{index + 1}.</span> {item.title}
                          </h3>
                        </div>
                        <button className="packege_right_btn" onClick={() => handleIsActive(index)}>
                          <AiOutlineRight className="faq_icon" />
                        </button>
                      </div>
                      {shown === index && (
                        <div className="packeges_bottom common_title">
                          <p>{item.content}</p>
                        </div>
                      )}
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </section>

        <section className={styles.our_result_sec}>
          <div className="container">
            <div className="row align-items-center justify-content-center">
              <div className="common_title text-center pt-5 pb-4 col-md-6">
                <h2>
                  Our <span className="fw-bold">Results</span>
                </h2>
                <p>
                  Take a look at what all the fuss is about. When it comes to managing and growing social media channels
                  for your business, AM Group are your go-to!
                </p>
              </div>
            </div>
          </div>
          <div className={styles.our_result_main}>
            <div className={styles.our_result_con}>
              <Image src="/icons/elpatron.svg" width={209} height={29}></Image>

              <h3>
                <span>200 </span> generated leads.
              </h3>
              <h3>
                <span>$7.58 </span> cost per lead.
              </h3>
              <h3>
                <span>8,150 </span> clicks.
              </h3>
              <h3>
                <span>$0.22 </span> per clicks.
              </h3>
            </div>
          </div>
        </section>
        <section className="faq_sec common_padding">
          <div className="container">
            <div className="faq_top common_title">
              <h2>
                Frequently Asked <br />
                <span className="fw-bold"> Questions.</span>
              </h2>
            </div>
            <div className="faq_main">
              <Accordion allowZeroExpanded className="accordion_main col-md-7">
                {faqs.map((item, index) => (
                  <AccordionItem className="accordion_card" key={index}>
                    <AccordionItemHeading>
                      <AccordionItemButton className="accordion_bg_orange">
                        <h4 className="accordion_title">{item.title}</h4>
                        <button className="accordion_btn">
                      <AiOutlineRight className="faq_icon" />
                    </button>
                      </AccordionItemButton>
                    </AccordionItemHeading>
                    <AccordionItemPanel className="accordion_panel">
                      <p className="accordion_desc">{item.description}</p>
                    </AccordionItemPanel>
                  </AccordionItem>
                ))}
              </Accordion>
            </div>
          </div>
          <div className="faq_btn_main">
            <Link href="/">
              <a className="faq_btn">Get Started</a>
            </Link>
          </div>
        </section>
      </Layout>
    </>
  );
};

export default Socialmediaadvertising;
