import React, { useState, useRef, useEffect } from "react";

import Link from "next/link";
import Image from "components/Image";
import Layout from "components/Layout";
import { AiOutlineRight } from "react-icons/ai";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from "react-accessible-accordion";

var $ = require("jquery");
if (typeof window !== "undefined") {
  window.$ = window.jQuery = require("jquery");
}

import dynamic from "next/dynamic";
const OwlCarousel = dynamic(() => import("react-owl-carousel"), {
  ssr: false,
});

import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import WebsitePageLogo from "components/Reusable/WebsitePageLogo/WebsitePageLogo";

const Websites = () => {
  const [shown, setIsShown] = useState(0);
  const [questions, setIsQuestions] = useState(null);

  const handleIsActive = (value) => {
    setIsShown(value);
  };
  const activeQuestions = (value) => {
    setIsQuestions(value);
  };

  const faqs = [
    {
      id: "0",
      title: " How long does it take to see results",
      description:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      id: "1",
      title: "Do I have to be local to work with you?",
      description:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      id: "2",
      title: "Are there any hidden agency fees?",
      description:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      id: "3",
      title: "How profitable are Facebook ads?",
      description:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      id: "4",
      title: "How much do I need to spend on Facebook?",
      description:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      id: "5",
      title: "06. Will I be locked in a contract?",
      description:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
    {
      id: "6",

      title: "07. Does AM Group create my content?",
      description:
        "There are many factors that contribute to quick success. If there’s strong campaigns/offers to work with, more than likely you’ll see results within the first week. If we are starting a new ad account for the business, Facebooks algorithim needs to go through the “learning phase”, which can sometimes takes a few weeks.",
    },
  ];
  const websites = [
    {
      bgImage: "/images/websites-img/web-img1.png",
      websiteName: "SB Chiro",
    },
    {
      bgImage: "/images/websites-img/web-img2.png",
      websiteName: "Krakow",
    },
    {
      bgImage: "/images/websites-img/web-img1.png",
      websiteName: "SB Chiro",
    },
    {
      bgImage: "/images/websites-img/web-img2.png",
      websiteName: "Krakow",
    },
    {
      bgImage: "/images/websites-img/web-img2.png",
      websiteName: "Krakow",
    },
  ];
  const web_slider = {
    loop: true,
    margin: 20,
    center: false,
    items: 4,
    nav: false,
    dots: true,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1.3,
        center: false,
        loop: true,
        dots: true,
      },
      600: {
        items: 2,
        loop: true,
        dots: true,
      },
      1000: {
        items: 4,
        loop: false,
        dots: true,
      },
    },
  };
  const packeges = [
    {
      title: "Responsive Design",
      content:
        "We understand the importance of a responsive design. Our design process ensures that every device is tested for a complete and faultless journey for your customer. Prior to launch, we complete thorough testing via a GO LIVE checklist, developed over years of experience, ensuring a smooth launch.",
    },
    {
      title: "Integrations",
      content:
        "We understand the importance of a responsive design. Our design process ensures that every device is tested for a complete and faultless journey for your customer. Prior to launch, we complete thorough testing via a GO LIVE checklist, developed over years of experience, ensuring a smooth launch.",
    },
    {
      title: "Enterprise",
      content:
        "We understand the importance of a responsive design. Our design process ensures that every device is tested for a complete and faultless journey for your customer. Prior to launch, we complete thorough testing via a GO LIVE checklist, developed over years of experience, ensuring a smooth launch.",
    },
  ];
  return (
    <>
      <Layout>
        <section className="banner services_banner common_padding">
          <div className="container">
            <div className="banner_content">
              <div className="row align-items-center  ">
                <div className="col-lg-8 col-md-8 col-md-12 common_title">
                  <p>
                    Website Design <span className="fw-bold">& Development</span>
                  </p>
                  <h1>
                    We Create Sites
                    <br /> <span className="orange_color fw-bold">That Convert</span>
                  </h1>
                </div>
                <div className="col-lg-4 col-md-4 col-md-12 services_content_right">
                  <p>
                    Considering it's your sales person that never sleeps, we specialise in building beautiful and
                    functional websites customised to what’s needed to help your business grow.
                  </p>
                </div>
              </div>
            </div>
            <div className="banner_image_column">
              <img src="/images/websites.png"></img>
            </div>
          </div>
        </section>

        <WebsitePageLogo />

        <section className="packeges_sec_main common_padding" id="marketing_packages">
          <div className="container">
            <div className="row align-items-center">
              <div className="col-md-6">
                <div className="packeges_sec_left common_title">
                  <p>
                    How We<span className="fw-bold"> Do It</span>
                  </p>
                  <h2>
                    Perfect Solutions For <br />
                    <span className="fw-bold">Your Business.</span>
                  </h2>
                </div>
              </div>

              <div className="col-md-6 col-sm-12">
                {packeges &&
                  packeges.map((item, index) => (
                    <div className={shown === index ? "bg_black packeges_sec_right" : "packeges_sec_right"} key={index}>
                      <div className="d-flex justify-content-between pb-2">
                        <div className="flex_center common_title">
                          <h3>
                            {index + 1}. {item.title}
                          </h3>
                        </div>
                        <button className="packege_right_btn" onClick={() => handleIsActive(index)}>
                          <AiOutlineRight className="faq_icon" />
                        </button>
                      </div>
                      {shown === index && (
                        <div className="packeges_bottom common_title">
                          <p>{item.content}</p>
                        </div>
                      )}
                    </div>
                  ))}
              </div>
            </div>
          </div>
        </section>

        <section className="websites_section common_padding">
          <div className="container">
            <div className="row">
              <div className="col-lg-6 col-md-6 col-sm-12 common_title text-left">
                <h2>
                  The Websites <br /> We Build
                  <span className="orange_color fw-bold"> Are Keepers.</span>
                </h2>
              </div>
              <div className="col-lg-5 offset-lg-1 col-md-5 offset-md-1 col-sm-12 common_title common_para text-left">
                <p>
                  Our process ensures that every device is tested for a complete and faultless journey. Experience how
                  functionality meets aesthetics with just a few of our many projects.
                </p>
              </div>
            </div>

            <div className="clearfix"></div>
            <div className="row websites_section_columns">
              <OwlCarousel responsiveClass="true" {...web_slider}>
                {websites &&
                  websites.map((item, index) => (
                    <div className="website_column" key={index}>
                      <div
                        className="website_column_image w-100"
                        style={{
                          backgroundImage: `url(${item.bgImage})`,
                        }}
                      ></div>
                      <button>
                        <p>{item.websiteName}</p>
                      </button>
                    </div>
                  ))}
              </OwlCarousel>
            </div>
          </div>
        </section>

        <section className="faq_sec faq_sec_orange common_padding">
          <div className="container">
            <div className="faq_top_white common_title">
              <p>
                Why Choose<span className="fw-bold"> AM Group</span>
              </p>
              <h2>
                Frequently Asked <span className="fw-bold">Questions.</span>
              </h2>
            </div>
            <div className="faq_main">
              <Accordion allowZeroExpanded className="accordion_main col-md-7">
                {faqs.map((item, index) => (
                  <AccordionItem className="accordion_card accordion_card_black" key={index}>
                    <AccordionItemHeading>
                      <AccordionItemButton className="accordion_bg_orange accordion_bg_black">
                        <h2 className="accordion_title">{item.title}</h2>
                        <button className="accordion_btn">
                      <AiOutlineRight className="faq_icon" />
                    </button>
                      </AccordionItemButton>
                    </AccordionItemHeading>
                    <AccordionItemPanel className="accordion_panel">
                      <p className="accordion_desc">{item.description}</p>
                    </AccordionItemPanel>
                  </AccordionItem>
                ))}
              </Accordion>
            </div>
            <div className="faq_btn_main">
              <Link href="/">
                <a className="faq_btn faq_btn_white">Get Started</a>
              </Link>
            </div>
          </div>
        </section>
      </Layout>
    </>
  );
};

export default Websites;
