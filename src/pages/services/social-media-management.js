import React, { useState } from "react";
import Link from "next/link";
import Image from "components/Image";
import Layout from "components/Layout";
import Socialmediapackages from "components/Reusable/SocialMediaPackage/SocialMediaPackage";
import TextImage from "components/Reusable/TextImage/TextImage";
import Solutionforbusiness from "components/Reusable/Solutionforbusiness/Solutionforbusiness";
import styles from "../../styles/pages/Social-media-management.module.scss";
import Ourclients from "components/Reusable/Ourclients/Ourclients";
import { AiOutlineRight } from "react-icons/ai";

var $ = require("jquery");
if (typeof window !== "undefined") {
  window.$ = window.jQuery = require("jquery");
}

import dynamic from "next/dynamic";
const OwlCarousel = dynamic(() => import("react-owl-carousel"), {
  ssr: false,
});

import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

const Socialmediamanagement = () => {
  const [shown, setIsShown] = useState(0);
  const handleIsActive = (value) => {
    setIsShown(value);
  };

  const packeges = [
    {
      title: "Professional",
      content:
        "You will receive a Social Media Manager, monthly content strategy, x6 weekly posts, content calendar, community management, monthly call, performance reports and 24/7 acces to a live analytics dashboard.",
      posts: "6 Weekly",
      management: "3 Hours Weekly",
    },
    {
      title: "Business",
      content:
        "You will receive a Social Media Manager, monthly content strategy, x6 weekly posts, content calendar, community management, monthly call, performance reports and 24/7 acces to a live analytics dashboard.",
      posts: "6 Weekly",
      management: "3 Hours Weekly",
    },
    {
      title: "Enterprise",
      content:
        "You will receive a Social Media Manager, monthly content strategy, x6 weekly posts, content calendar, community management, monthly call, performance reports and 24/7 acces to a live analytics dashboard.",
      posts: "6 Weekly",
      management: "3 Hours Weekly",
    },
  ];
  const logoSlider = {
    loop: false,
    margin: 20,
    // items: 4,
    nav: false,
    dots: false,
    responsiveClass: true,
    responsive: {
      0: {
        items: 3,
        dots: false,
        loop: false,
      },
      600: {
        items: 3,
        dots: false,
        loop: false,
      },
      1000: {
        items: 3,
        dots: false,
        loop: false,
      },
    },
  };
  const managementLogo = [
    {
      logoImage: "/icons/management/fb.svg",
    },
    {
      logoImage: "/icons/management/insta.svg",
    },
    {
      logoImage: "/icons/management/linkedin.svg",
    },
  ];
  return (
    <>
      <Layout>
        <section className="banner services_banner common_padding">
          <div className="container">
            <div className="banner_content">
              <div className="row align-items-center">
                <div className="col-lg-8 col-md-8 col-md-12 services_content_left common_title">
                  <p>
                    Social Media<span className="fw-bold"> Management</span>
                  </p>
                  <h1>
                    Increase &<span className="orange_color fw-bold"> Boost</span>
                    <br />
                    Your Traffic
                  </h1>
                </div>
                <div className="col-lg-4 col-md-4 col-md-12 services_content_right">
                  <p>
                    Produce and automate a scroll-stopping social media page that speaks directly to an engaged
                    audience. Forget likes or followers, think conversations that lead to conversions!
                  </p>
                </div>
              </div>
            </div>
            <div className="banner_image_column">
              <img src="/images/text-image.jpg"></img>
            </div>
          </div>
        </section>

        <div className="common_padding text-center pt-0">
          <div className="container">
            <OwlCarousel responsiveClass="true" {...logoSlider}>
              {managementLogo.map((logo, index) => (
                <div className="logo_slider" key={index}>
                  <Image src={logo.logoImage} width={120} height={50}></Image>
                </div>
              ))}
            </OwlCarousel>
          </div>
        </div>

        <section className="packeges_sec_main common_padding" id="marketing_packages">
          <div className="container">
            <div className="row align-items-center">
              <div className="col-md-6">
                <div className="packeges_sec_left common_title">
                  <p>
                    Our<span className="fw-bold"> Packages</span>
                  </p>
                  <h2>
                    Social Media Marketing <br />
                    <span className="fw-bold">Packages.</span>
                  </h2>
                </div>
              </div>

              <div className="col-md-6 col-sm-12">
                {packeges &&
                  packeges.map((item, index) => (
                    <div className={shown === index ? "bg_black packeges_sec_right" : "packeges_sec_right"} key={index}>
                      <div className="d-flex justify-content-between pb-2">
                        <div className="flex_center common_title">
                          <h3>
                            {index + 1}. {item.title}
                          </h3>
                        </div>
                        <button className="packege_right_btn" onClick={() => handleIsActive(index)}>
                          <AiOutlineRight className="faq_icon" />
                        </button>
                      </div>
                      {shown === index && (
                        <div className="packeges_bottom common_title">
                          <p>{item.content}</p>
                          <div className="packeges_bottom_child">
                            <p>
                              Quantity of posts:&nbsp;
                              <span className="orange_color">{item.posts}</span>
                            </p>
                            <p>
                              Community Management:&nbsp;
                              <span className="orange_color">{item.management}</span>
                            </p>
                          </div>
                        </div>
                      )}
                    </div>
                  ))}
              </div>
            </div>
          </div>
        </section>
        <Solutionforbusiness />

        <section className={`${styles.insta_img} common_padding`}>
          <div className="container">
            <div className="row justify-content-center">
              <div className={styles.insta_img_sec}>
                <div className={styles.insta_img_main}></div>
                <div className={styles.insta_img_main2}></div>
              </div>
            </div>
          </div>
        </section>

        <div className={`${styles.content_sec} common_padding pt-0`}>
          <div className="container">
            <div className={`${styles.content_main} row`}>
              <div className={`${styles.content_main_left} col-md-6 col-sm-6`}>
                <div className={`${styles.content_text_left} common_title `}>
                  <h2>
                    Content <br />
                    <span className="fw-bold">Calendar.</span>
                  </h2>
                  <p>
                    With a dedicated Social Media Account Manager, they’ll work closely with our network of content
                    creators and graphic designers to execute and plan an effective social media marketing strategy that
                    is straight forward, automated and on-brand. {"\n \n"} We’ll produce and automate a scroll-stopping
                    social media page that speaks directly to an engaged audience, so you’ll never need to put your
                    social media strategy on the back-burner again.
                  </p>
                  <Link href="/">
                    <a className="common_btn">Let's Chat</a>
                  </Link>
                </div>
              </div>
              <div className={`${styles.content_main_right} col-md-6 col-sm-6`}>
                <div style={{ backgroundImage: `url("/images/contentimg1.png")` }} className={styles.left_image}></div>
              </div>
            </div>

            <div className={`${styles.content_main} row`}>
              <div className={`${styles.content_main_right} col-md-6 col-sm-6`}>
                <div style={{ backgroundImage: `url("/images/contentimg2.png")` }} className={styles.left_image2}>
                  <button>
                    <Image src="/clients-logo/minto.svg" width={98} height={32}></Image>
                  </button>
                </div>
              </div>
              <div className={`${styles.content_main_left} ${styles.left_order} col-md-6 col-sm-6`}>
                <div className={`${styles.content_text_right} common_title `}>
                  <h2>
                    Community <br />
                    <span className="fw-bold"> Management.</span>
                  </h2>
                  <p>
                    In basic terms – We’ll make life SO much easier for you! We’ll spend a minimum of 100 minutes per
                    week on your platforms, engaging with, liking and commenting on your audience’s content and other
                    relevant brands/individuals. Algorithm’s are constantly changing, so we’ll adapt community
                    management techniques to align with the current social media climate. This aspect of SMM will help
                    grow your audience and keep your community engaged and entertained.
                  </p>
                  <Link href="/">
                    <a className="common_btn">Let's Chat</a>
                  </Link>
                </div>
              </div>
            </div>

            <div className={`${styles.content_main} row pt-0 pb-0`}>
              <div className={`${styles.content_main_left} col-md-6 col-sm-6`}>
                <div className={`${styles.content_text_left} common_title `}>
                  <h2>
                    Reporting & <br />
                    <span className="fw-bold">Analytics.</span>
                  </h2>
                  <p>
                    AM GROUP understands and keeps on top of the ever-evolving social media marketing landscape, so you
                    don’t have to. We’ll create a tailored marketing strategy for your brand that will drive traffic to
                    your website. Our clients can login to an online portal at anytime to see live reporting on their
                    marketing results. Imagine having 24/7 access to see where every dollar is spent!
                  </p>
                  <Link href="/">
                    <a className="common_btn">Let's Chat</a>
                  </Link>
                </div>
              </div>
              <div className={`${styles.content_main_right} ${styles.hideon_mobile} col-md-6 col-sm-6`}>
                <div style={{ backgroundImage: `url("/images/contentimg1.png")` }} className={styles.left_image}></div>
              </div>
            </div>
          </div>
        </div>
        <Ourclients />
        <section className={`${styles.certified_Section} common_padding`}>
          <div className="container">
            <div className={`${styles.ourwork_main} row`}>
              <div className="col-md-6 col-sm-12">
                <div className="common_title text-left w-100">
                  <h2 className="text-capitalize">
                    A Glance at
                    <br />
                    <span className="fw-bold"> Our Work</span>
                  </h2>
                  <p>
                    Excogitatum est super his, ut homines uid psa pavendi coll igendos rumores per Ant iochiae latera
                    inatur relaturiExcogitatume st super his ut homines uida
                  </p>
                  <Link href="/">
                    <a className="common_btn">Case Studies</a>
                  </Link>
                </div>
              </div>
              <div className={`${styles.ourwork_right} col-md-6 col-sm-12`}>
                <div className={styles.ourwork_image}></div>
              </div>
            </div>
          </div>
        </section>
      </Layout>
    </>
  );
};

export default Socialmediamanagement;
