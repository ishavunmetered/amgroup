import react, { useRef, useEffect, useLayoutEffect } from "react";
import Link from "next/link";
import Image from "next/image";
import Head from "next/head";
import { gsap } from "gsap/dist/gsap";
import { SplitText } from "gsap/dist/SplitText";
import Loader from "components/Loader";

if (process.client) {
  gsap.registerPlugin(SplitText);
}

import Layout from "components/Layout";
// import Header from 'components/Header';
import Section from "components/Section";
import Container from "components/Container";

import styles from "styles/pages/Home.module.scss";
import { useState } from "react";

var $ = require("jquery");
if (typeof window !== "undefined") {
  window.$ = window.jQuery = require("jquery");
}

import dynamic from "next/dynamic";
const OwlCarousel = dynamic(() => import("react-owl-carousel"), {
  ssr: false,
});

import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

const bussiness_owl = {
  loop: false,
  margin: 20,
  items: 7,
  nav: false,
  dots: false,
  responsiveClass: true,
  responsive: {
    0: {
      items: 4,
      dots: false,
      loop: false,
    },
    600: {
      items: 4,
      dots: false,
      loop: false,
    },
    1000: {
      items: 7,
      dots: false,
      loop: false,
    },
  },
};
const marketing_owl = {
  loop: false,
  margin: 20,
  // items: 3,
  nav: false,
  dots: false,
  responsiveClass: true,
  responsive: {
    0: {
      items: 1.3,
      dots: false,
      loop: true,
    },
    600: {
      items: 2,
      dots: false,
      loop: true,
    },
    1000: {
      items: 3,
      dots: false,
      loop: false,
    },
  },
};

export default function Home() {
  let content = useRef(null);
  const [shown, setIsShown] = useState(null);
  const [buttonPopup, setButtonpopup] = useState(false);

  const handleIsActive = (value) => {
    setIsShown(value);
    setButtonpopup(!buttonPopup);
  };

  const bussinessLogoData = [
    {
      logoImage: "/business-logo/insta.png",
    },
    {
      logoImage: "/business-logo/meta.png",
    },
    {
      logoImage: "/business-logo/linkedin.svg",
    },
    {
      logoImage: "/business-logo/tiktok.svg",
    },
    {
      logoImage: "/business-logo/sprout.svg",
    },
    {
      logoImage: "/business-logo/google-partner.png",
    },
    {
      logoImage: "/business-logo/shopify-partners.png",
    },
  ];

  const mobilebussLogoData = [
    {
      logoImage: "/business-logo/shopify-white.png",
    },
    {
      logoImage: "/business-logo/mailchimp-white.png",
    },
    {
      logoImage: "/business-logo/facebook.svg",
    },
    {
      logoImage: "/business-logo/google-white.png",
    },
  ];

  return (
    <>
     <Head>
        <title>AM Group</title>
        <link rel="shortcut icon" href="/favicon.png" />
    </Head>
    <Layout>
      <Section>
        <Container>
          <div className="hero_section_desktop common_padding">
            <div className="row align-items-center">
              <div
                className={`${styles.top_sec_left} col-sm-12 col-md-8 col-lg-8 common_title`}
                ref={(el) => (content = el)}
              >
                <div>
                  <h1 className="animate-title">building</h1>
                  <h1 className="animate-title">
                    <span className="orange_color">brands</span> online<span className="orange_color">.</span>
                    <span className={styles.vector_image}>
                      <Image width={200} height={40} src="/icons/Vector.svg" alt="logo"></Image>
                    </span>
                  </h1>
                </div>
              </div>
              <div className="col-sm-12 col-md-4 col-lg-4 top_sec_right">
                <h2>Award-Winning Digital Marketing Agency.</h2>
                <Link href="/">
                  <a className="common_btn">Get In Touch</a>
                </Link>
              </div>
            </div>
          </div>
        </Container>
      </Section>

      <Section>
        <div className="hero_section_mobile common_padding">
          <div className="row text-center">
            <Link href="/">
              <a className={styles.mobile_logo}>
                <Image src="/icons/logo-white.svg" width={121} height={50} alt="icon"></Image>
              </a>
            </Link>
            <div className={styles.mobile_banner} style={{ backgroundImage: `url("/mobile-banner.png")` }}>
              <div className={styles.mobile_text}>
                <Container>
                  <h1 className={styles.mobile_title}>
                    building<br></br>
                    brands
                    <br /> <span>online</span>.
                  </h1>
                </Container>
              </div>
            </div>
            <Container>
              <div className={`${styles.mobile_sec} col-sm-12 common_title`}>
                <Link href="/">
                  <a className={styles.mobile_cmn_btn}>get in touch</a>
                </Link>
                <div className={`${styles.mobile_business_sec} common_padding text-center pb-0`}>
                  <h3 className={styles.mobile_business_sub}>Our Expertise</h3>
                  <div className={`${styles.mobile_business_logos} row`}>
                    <OwlCarousel responsiveClass="true" {...bussiness_owl}>
                      {mobilebussLogoData.map((logo, index) => (
                        <div key={index}>
                          <Image src={logo.logoImage} width="100%" height="100%" objectFit="contain" alt="icon"></Image>
                        </div>
                      ))}
                    </OwlCarousel>
                  </div>
                </div>
                <h1>hack online growth</h1>
                <div className={styles.mobile_review_small}>
                  <Image width={45} height={41} src="/icons/google-icon.svg" alt="icon"></Image>
                  <div className={styles.rating_text_main}>
                    <span className={styles.rating_text}>Google Rating</span>
                    <span className={styles.rating_text}>
                      4.9 <Image width={15} height={15} src="/icons/google-rating.svg" alt="icon"></Image>
                      <Image width={15} height={15} src="/icons/google-rating.svg" alt="icon"></Image>
                      <Image width={15} height={15} src="/icons/google-rating.svg" alt="icon"></Image>
                      <Image width={15} height={15} src="/icons/google-rating.svg" alt="icon"></Image>
                      <Image width={15} height={15} src="/icons/google-rating.svg" alt="icon"></Image>
                    </span>
                  </div>
                </div>
              </div>
            </Container>
          </div>
        </div>
      </Section>

      <Section className={styles.page_banner}>
        <div className={styles.banner_img}>
        </div>
        {/* <video autoPlay muted loop className={styles.video_ban}>
          <source src="/video/am-video.mp4" type="video/mp4" />
        </video> */}
        {/* <div className={styles.page_banner}>

        </div> */}
        <Container className={styles.banner_container}>
          <div className={styles.review_small}>
            <Image width={50} height={50} src="/icons/google-icon.svg" alt="icon"></Image>
            <div className={styles.rating_text_main}>
              <span className={styles.rating_text}>Google Rating</span>
              <span className={styles.rating_text}>
                4.9 <Image width={15} height={15} src="/icons/google-rating.svg" alt="icon"></Image>
                <Image width={15} height={15} src="/icons/google-rating.svg" alt="icon"></Image>
                <Image width={15} height={15} src="/icons/google-rating.svg" alt="icon"></Image>
                <Image width={15} height={15} src="/icons/google-rating.svg" alt="icon"></Image>
                <Image width={15} height={15} src="/icons/google-rating.svg" alt="icon"></Image>
              </span>
            </div>
          </div>
        </Container>
      </Section>

      <Section className={`${styles.business_sec} common_padding text-center`}>
        <Container>
          <h3 className={styles.business_subtitle}>Our Expertise </h3>
          <div className={`${styles.business_logos} row`}>
            <OwlCarousel responsiveClass="true" {...bussiness_owl}>
              {bussinessLogoData.map((logo, index) => (
                <div key={index}>
                  <Image src={logo.logoImage} width="100%" height="100%" objectFit="contain" alt="icon"></Image>
                </div>
              ))}
            </OwlCarousel>
          </div>
        </Container>
      </Section>

      <Section className={`${styles.adv_sec} common_padding pt-0`}>
        <Container>
          <div className={`${styles.ser_main} common_title text-center`}>
            <h2>
              hack<span className="orange_color"> online </span>growth<span>.</span>
            </h2>
          </div>
        </Container>

        <div className="row w-100 service_main">
          <div className={`${styles.ser_left} col-sm-12 col-md-4 p_0`}>
            <div className={`ser_left_item ${styles.ser_left_item1}`}>
              <h2>
                Facebook
                <br /> Advertising.
              </h2>
            </div>
          </div>
          <div className="col-sm-12 col-md-8 p_0">
            <div className={`ser_right_item ${styles.ser_right_item1}`}></div>
            <div className={styles.item_title}>Facebook Advertising</div>
          </div>
        </div>
        <div className="row w-100 service_main">
          <div className={`${styles.ser_left} col-sm-12 col-md-4 p_0`}>
            <div className={`ser_left_item ${styles.ser_left_item2}`}>
              <h2>
                Social
                <br /> Media <br /> Experts
              </h2>
            </div>
          </div>
          <div className="col-sm-12 col-md-8 p_0">
            <div className={`ser_right_item ${styles.ser_right_item2}`}></div>
            <div className={styles.item_title}>Social Media Management</div>
          </div>
        </div>
        <div className="row w-100 service_main">
          <div className={`${styles.ser_left} col-sm-12 col-md-4 p_0`}>
            <div className={`ser_left_item ${styles.ser_left_item3}`}>
              <h2>
                Custom
                <br /> Websites.
              </h2>
            </div>
          </div>
          <div className="col-sm-12 col-md-8 p_0">
            <div className={`ser_right_item ${styles.ser_right_item3}`}></div>
            <div className={styles.item_title}>Website Design & Development</div>
          </div>
        </div>
      </Section>

      <Section className={styles.awards_sec}>
        <Container className={styles.awards_sec_con}>
          <div className="common_title">
            <h2 className="animate-title">award winning agency.</h2>
          </div>
          <div className="mar_carousel row common_padding">
            <OwlCarousel responsiveClass="true" {...marketing_owl}>
              <div className={`${styles.marketer_main}`}>
                <div className={`${styles.mar_main_left} col-md-6 col-sm-6`}>
                  <Image src="/icons/mar-logo-1.svg" width={161} height={212} alt="icon"></Image>
                </div>
                <div className={`${styles.mar_main_right} col-md-6 col-sm-6`}>
                  <span>
                    <strong>2020</strong>
                  </span>
                  <p className={`${styles.mar_para} child-desc`}>
                    AM Group beat a huge list of worthy finalists to take home the award for the best Professional
                    Service in the area.
                  </p>
                </div>
              </div>
              <div className={`${styles.marketer_main}`}>
                <div className={`${styles.mar_main_right} col-md-6 col-sm-6`}>
                  <Image src="/icons/mar-logo-2.svg" width={161} height={212} alt="icon"></Image>
                </div>
                <div className={`${styles.mar_main_right} col-md-6 col-sm-6`}>
                  <span>
                    <strong>2022, 2021 & 2019</strong>
                  </span>
                  <p className={`${styles.mar_para} child-desc`}>
                    We see this award as the ultimate recognition of who we are and what we’ve achieved so far, year
                    after year.
                  </p>
                </div>
              </div>
              <div className={`${styles.marketer_main}`}>
                <div className={`${styles.mar_main_right} col-md-6 col-sm-6`}>
                  <Image src="/icons/mar-logo-3.svg" width={147} height={102} alt="icon"></Image>
                  <Image src="/icons/mar-logo-4.svg" width={147} height={102} alt="icon"></Image>
                </div>
                <div className={`${styles.mar_main_right} col-md-6 col-sm-6`}>
                  <span>
                    <strong>2021</strong>
                  </span>
                  <p className={`${styles.mar_para} child-desc`}>
                    The year we won two prestigious awards. Our Digital Marketing success has been recognised by some of
                    the best!
                  </p>
                </div>
              </div>
            </OwlCarousel>
          </div>
        </Container>
      </Section>

      <Section className="common_padding pt-0">
        <div style={{ backgroundImage: `url("/mobile-bg-image.png")` }} className={styles.mobile_bg_image}></div>
        <Container>
          <div className="row align-items-center">
            <div className={`${styles.marketing_right} col-sm-12 col-md-6`}>
              <div className="img_reveal_main">
                <div className={`${styles.marketing_right_image} img_reveal`}></div>
              </div>
            </div>
            <div className={`${styles.markeing_left} col-sm-12 col-md-6`}>
              <h2 className="">
                Talk To A <br />
                <strong>Marketing Specialist</strong>
                <br />
                Today
              </h2>
              <div className={styles.mobile_cont_detail}>
                <h3>Talk To A Marketing Specialist Today</h3>
                <div className="common_title">
                  <div className={styles.con_details}>
                    <a href="tel:0286464909">
                      <span className={styles.icon_span}>
                        <Image src="/icons/phone.svg" width={24} height={24} alt="icon"></Image>
                      </span>
                      02 8646 4909
                    </a>
                    <a href="mailto:info@am-group.com.au" className={styles.right_main_btn}>
                      <span className={styles.icon_span}>
                        <Image src="/icons/mail.svg" width={25} height={18} alt="icon"></Image>
                      </span>
                      info@am-group.com.au
                    </a>
                  </div>
                </div>
              </div>
              <div className={styles.desktop_par_left_item}>
                <form action="" className={`${styles.form_control} row`}>
                  <div className={`${styles.name_input} col-md-6`}>
                    <input type="text" id="fullname" required />
                    <label htmlFor="fullname">Your name:</label>
                  </div>
                  <div className={`${styles.name_input} col-md-6`}>
                    <input type="number" id="phone" required />
                    <label htmlFor="phone">Phone Number</label>
                  </div>
                  <div className="clearfix"></div>
                  <div className={`${styles.name_input} col-md-6`}>
                    <input type="text" id="email" required />
                    <label htmlFor="email">Your Email Address</label>
                  </div>
                  <div className={`${styles.name_input} col-md-6`}>
                    <input type="text" id="companyname" required />
                    <label htmlFor="companyname">Company Name</label>
                  </div>
                  <div className={`${styles.name_input} col-md-12`}>
                    <input type="text" id="services" required />
                    <label htmlFor="services">Select Your Service</label>
                  </div>
                  <div className="clearfix"></div>
                  <div className={`${styles.name_input} col-md-12`}>
                    <input type="text" id="messageforus" required />
                    <label htmlFor="messageforus">Message</label>
                  </div>
                  <div className="clearfix"></div>
                  <div>
                    <button type="submit" className={`${styles.form_btn} common_btn`}>
                      Submit
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </Container>
      </Section>
    </Layout>
    </>
  );
}
