import Image from "next/image";
import Link from "next/link";

import Container from "components/Container";

import styles from "./Footer.module.scss";

const Footer = () => {
  return (
    <footer className={styles.footer_section} id="footer">
      <div className={`${styles.footer_upper} w-100`}>
        <Container>
          <div className="row">
            <div className={`${styles.col} ${styles.footer_column} `}>
              <a className={styles.mail_head}>
                We are a full service marketing solution based in Sydney. We’re a young passionate team doing big
                things. Through innovation and passion, we build brands that dominate the digital space.
              </a>
            </div>
            <div className={`${styles.col} ${styles.footer_column} `}>
              <h3>Information</h3>
              <div>
                <ul>
                  <li>
                    <a>Get in touch</a>
                  </li>
                </ul>
                <a href="mailto:info@am-group.com.au">info@am-group.com.au</a>
              </div>
              <div>
                <ul>
                  <li>
                    <a className={styles.mail_head}>Call us</a>
                  </li>
                </ul>
                <a href="tel:02 8646 4909">02 8646 4909</a>
              </div>
            </div>
            <div className={`${styles.col} ${styles.footer_column} `}>
              <h3>Office Location</h3>
              <ul>
                <li>
                  <a>Unit 2208 31b Lasso Road, Gregory Hills 2557</a>
                </li>
                <li>
                  <a>NSW, Australia</a>
                </li>
              </ul>
            </div>
            <div className={`${styles.col} ${styles.footer_column} `}>
              <h3>Services</h3>
              <ul>
                <li>
                  <Link href="/social">
                    <a>Social Media Management</a>
                  </Link>
                </li>
                <li>
                  <Link href="/websites">
                    <a>Websites</a>
                  </Link>
                </li>
                <li>
                  <Link href="/content">
                    <a>Content Creation</a>
                  </Link>
                </li>
              </ul>
            </div>
            {/* <div className={`${styles.col} ${styles.footer_column} `}>
              <h3>Quick Links</h3>
              <ul>
                <li>
                  <Link href="/about">
                    <a>About us</a>
                  </Link>
                </li>
                <li>
                  <Link href="/services">
                    <a>Services</a>
                  </Link>
                </li>
                <li>
                  <Link href="/case">
                    <a>Case Studies</a>
                  </Link>
                </li>
                <li>
                  <Link href="/news">
                    <a>News</a>
                  </Link>
                </li>
                <li>
                  <Link href="/contact">
                    <a>Contact</a>
                  </Link>
                </li>
              </ul>
            </div> */}
          </div>
        </Container>
      </div>
      <div className={`${styles.footer_bottom} w-100`}>
        <Container>
          <div className={styles.mobile_fot_logo}>
            <Image src="/icons/mobile-ftr-logo.svg" width={76} height={32} alt="logo"></Image>
          </div>
          <div className={styles.ftr_logo}>
            <Image src="/icons/ftr-logo.svg" width={135} height={56} alt="logo"></Image>
          </div>

          <div className="row">
            <div className={`${styles.footer_bottom_column} col-lg-6 col-md-6`}>
              <p>© 2020 Affirmative Marketing Group</p>
            </div>
            <div className={`${styles.footer_bottom_column} col-lg-6 col-md-6 `}>
              <Link href="https://www.facebook.com/affirmativemarketing/">
                <a target="_blank" rel="noreferrer">
                  <Image src="/icons/fb-logo.png" width={30} height={30} alt="icon"></Image>
                </a>
              </Link>
              <Link href="https://www.instagram.com/_amgroup/">
                <a target="_blank" rel="noreferrer">
                  <Image src="/icons/insta-logo.png" width={30} height={30} alt="icon"></Image>
                </a>
              </Link>
              <Link href="/">
                <a target="_blank" rel="noreferrer">
                  <Image src="/icons/tiktok-logo.png" width={30} height={30} alt="icon"></Image>
                </a>
              </Link>
            </div>
          </div>
        </Container>
      </div>
    </footer>
  );
};

export default Footer;
